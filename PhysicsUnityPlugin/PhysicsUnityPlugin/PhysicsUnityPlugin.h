#ifndef PHYSICS_UNITY_PLUGIN_H
#define PHYSICS_UNITY_PLUGIN_H

#include "Lib.h"

#ifdef __cplusplus
extern "C"
{
#else

#endif


PHYSICS_UNITY_PLUGIN_SYMBOL int InitFoo(int f_new);
PHYSICS_UNITY_PLUGIN_SYMBOL int DoFoo(int bar);
PHYSICS_UNITY_PLUGIN_SYMBOL int TermFoo();

	
#ifdef __cplusplus
}
#else

#endif

#endif