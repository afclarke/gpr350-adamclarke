﻿public enum Shape
{
    Disc,
    Ring,
    Rectangle,
    Rod
}

public enum Shape3D
{
    SolidBox,
    HollowBox,
    SolidSphere,
    HollowSphere,
    SolidCylinder,
    SolidCone
}
