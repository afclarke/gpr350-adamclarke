﻿using UnityEngine;

public static class Mafs
{
    // map deg to range of (0, 360]
    public static float MapToUnsigned360(float deg)
    {
        if (deg >= 360)
        {
            deg -= 360;
        }
        else if (deg < 0)
        {
            deg += 360;
        }

        return deg;
    }

    // map deg to range of [-180, 180)
    public static float MapToSigned180(float deg)
    {
        if (deg > 180)
        {
            deg -= 360;
        }
        else if (deg <= -180)
        {
            deg += 360;
        }

        return deg;
    }

    // get z axis rotation matrix for given angle in radians
    public static Matrix4x4 getZRotMat(float radians)
    {
        // cos(rad) -sin(rad) 0 0
        // sin(rad)  cos(rad) 0 0
        //    0          0    1 0
        //    0          0    0 1
        Matrix4x4 zRotMat = Matrix4x4.identity;
        float cosRot = Mathf.Cos(radians);
        float sinRot = Mathf.Sin(radians);
        zRotMat.m00 = cosRot;
        zRotMat.m10 = sinRot;
        zRotMat.m01 = -sinRot;
        zRotMat.m11 = cosRot;
        return zRotMat;
    }

    // rotate point around pivot on z axis
    public static Vector2 ZRotateAroundPivot(Vector2 point, Vector2 pivot, float radians)
    {
        point -= pivot;
        Matrix4x4 zRotMat = getZRotMat(radians);
        point = zRotMat.MultiplyVector(point);
        point += pivot;
        return point;
    }

    // rotate point around pivot on z axis
    public static Vector2 ZRotateAroundPivotInv(Vector2 point, Vector2 pivot, float radians)
    {
        point -= pivot;
        Matrix4x4 zRotMat = getZRotMat(radians).inverse;
        point = zRotMat.MultiplyVector(point);
        point += pivot;
        return point;
    }
}

