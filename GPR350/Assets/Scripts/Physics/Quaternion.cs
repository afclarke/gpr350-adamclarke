﻿
using Matrix4x4 = UnityEngine.Matrix4x4;
using Vector3 = UnityEngine.Vector3;
using Vector4 = UnityEngine.Vector4;

namespace Yeet
{
    public class Quaternion
    {
        private UnityEngine.Vector4 quaternion;

        Quaternion(float x, float y, float z, float w)
        {
            quaternion = new Vector4(x, y, z, w);
        }

        public static implicit operator UnityEngine.Quaternion(Yeet.Quaternion quaternion)
        {
            return new UnityEngine.Quaternion(quaternion.X, quaternion.Y, quaternion.Z, quaternion.W);
        }

        public static implicit operator Yeet.Quaternion(UnityEngine.Quaternion quaternion)
        {
            return new Yeet.Quaternion(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
        }

        public static implicit operator UnityEngine.Matrix4x4(Yeet.Quaternion quaternion)
        {
            Vector4 rotSqr = new Vector4(quaternion.X * quaternion.X, quaternion.Y * quaternion.Y, quaternion.Z * quaternion.Z, quaternion.W * quaternion.W);
            return new Matrix4x4(
                new Vector4(rotSqr.w + rotSqr.x - rotSqr.y - rotSqr.z, 2 * (quaternion.X * quaternion.Y + quaternion.W * quaternion.Z), 2 * (quaternion.X * quaternion.Z - quaternion.W * quaternion.Y), 0),
                new Vector4(2 * (quaternion.X * quaternion.Y - quaternion.W * quaternion.Z), rotSqr.w - rotSqr.x + rotSqr.y - rotSqr.z, 2 * (quaternion.Y * quaternion.Z + quaternion.W * quaternion.X), 0),
                new Vector4(2 * (quaternion.X * quaternion.Z + quaternion.W * quaternion.Y), 2 * (quaternion.Y * quaternion.Z - quaternion.W * quaternion.X), rotSqr.w - rotSqr.x - rotSqr.y + rotSqr.z, 0),
                new Vector4(0, 0, 0, 1));
        }

        // TODO
        //public static implicit operator Vector3(Yeet.Quaternion quaternion)
        //{
        //    Vector3 eulerAngles = Vector3.zero;
        //    eulerAngles.x = ;
        //    eulerAngles.y = ;
        //    eulerAngles.z = ;
        //    return eulerAngles;
        //}

        public float X
        {
            get => quaternion.x;
            private set => quaternion.x = value;
        }

        public float Y
        {
            get => quaternion.y;
            private set => quaternion.y = value;
        }

        public float Z
        {
            get => quaternion.z;
            private set => quaternion.z = value;
        }

        public float W
        {
            get => quaternion.w;
            private set => quaternion.w = value;
        }

        public static Quaternion operator *(Quaternion quaternion, float scalar)
        {
            quaternion.X *= scalar;
            quaternion.Y *= scalar;
            quaternion.Z *= scalar;
            quaternion.W *= scalar;
            return quaternion;
        }

        public static Quaternion operator *(Vector3 lhs, Quaternion rhs)
        {
            // vec3 * quat = (vec3, 1) * quat
            // product = ( newV, newW)
            // newW = lhs.W * rhs.W - dot(lhs.V, rhs.V)
            // newV = lhs.W * rhs.V + rhs.W * lhs.V + cross(lhs.V, rhs.V)

            // verbose what's going on
            //Vector3 rhsVector = new Vector3(rhs.X, rhs.Y, rhs.Z);
            //Quaternion lhsQuaternion = new Quaternion(lhs.x, lhs.y, lhs.z, 0);
            //var newW = lhsQuaternion.W * rhs.W - Vector3.Dot(lhs, rhsVector);
            //var newV = lhsQuaternion.W * rhsVector + rhs.W * lhs + Vector3.Cross(lhs, rhsVector);
            //return new Quaternion(newV.x, newV.y, newV.z, newW);

            // remove calculations that are always 0
            Vector3 rhsVector = new Vector3(rhs.X, rhs.Y, rhs.Z);
            var newW = -Vector3.Dot(lhs, rhsVector);
            var newV = rhs.W * lhs + Vector3.Cross(lhs, rhsVector);
            return new Quaternion(newV.x, newV.y, newV.z, newW);
        }

        public static Quaternion operator +(Quaternion lhs, Quaternion rhs)
        {
            return new Quaternion(
                lhs.X + rhs.X,
                lhs.Y + rhs.Y,
                lhs.Z + rhs.Z,
                lhs.W + rhs.W
            );
        }

        public void Normalize()
        {
            quaternion.Normalize();
        }
    }
}