﻿using System;
using UnityEngine;

// lab 4 step 1: collision hulls
public abstract class CollisionHull2D : MonoBehaviour
{
    public enum CollisionHullType2D
    {
        Circle,
        AABB,
        OBB
    }
    private CollisionHullType2D type { get; }

    protected CollisionHull2D(CollisionHullType2D _type)
    {
        type = _type;
    }

    public Particle2D particle { get; private set; }

    public Vector2 offset;
    public Vector2 center => (Vector2)transform.position + offset;

    // TODO: restituionAvg
    //public ResitutionCombineMethod combineMethod = AVERAGE;
    [Range(0, 1)]
    public float coefficientOfRestitution;

    public event EventHandler<Collision2D> OnCollisionEvent;

    private void Awake()
    {
        OnCollisionEvent = (sender, collision) => { };
    }

    private void Start()
    {
        particle = GetComponent<Particle2D>();
    }

    public static bool TestCollision(CollisionHull2D a, CollisionHull2D b, ref Collision2D collision2D)
    {
        if (a.particle == b.particle) return false;

        bool status = false;
        switch (b.type)
        {
            case CollisionHullType2D.Circle:
                status = a.TestCollisionVsCircle((CircleHull2D)b, ref collision2D);
                break;
            case CollisionHullType2D.AABB:
                status = a.TestCollisionVsAABB((AxisAlignedBoundingBoxHull2D)b, ref collision2D);
                break;
            case CollisionHullType2D.OBB:
                status = a.TestCollisionVsOBB((ObjectBoundingBoxHull2D)b, ref collision2D);
                break;
        }
        return status;
    }

    // lab 4 step 2
    public abstract bool TestCollisionVsCircle(CircleHull2D _other, ref Collision2D collision2D);
    public abstract bool TestCollisionVsAABB(AxisAlignedBoundingBoxHull2D _other, ref Collision2D collision2D);
    public abstract bool TestCollisionVsOBB(ObjectBoundingBoxHull2D _other, ref Collision2D collision2D);

    public void OnCollision(Collision2D collision2D)
    {
        OnCollisionEvent?.Invoke(this, collision2D);
    }
}
