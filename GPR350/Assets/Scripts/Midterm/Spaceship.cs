﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spaceship : MonoBehaviour
{
    public Vector2 flyRightForce = new Vector2(2, 2);
    public Vector2 flyLeftForce = new Vector2(-2, 2);

    private Particle2D particle;

    private Vector2 forceGravity = Vector2.zero;

    private void Start()
    {
        particle = GetComponent<Particle2D>();

        CircleHull2D hull = GetComponent<CircleHull2D>();
        hull.OnCollisionEvent += OnCircleHullCollision;
    }

    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            particle.AddForce(flyRightForce);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            particle.AddForce(flyLeftForce);
        }

        forceGravity = ForceGenerator.Gravity(particle.Mass, particle.gravityConstant, particle.gravityUpDir);
        particle.AddForce(forceGravity);
    }

    private void OnCircleHullCollision(object sender, Collision2D collision2D)
    {
        if (collision2D.b.gameObject.CompareTag("Finish"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        Vector2 forceOpposing = Vector2.zero;
        if (Mathf.Abs(collision2D.contact2D.normal.y) == 1)
        {
            forceOpposing.x = -particle.velocity.x;
        }
        else if(Mathf.Abs(collision2D.contact2D.normal.x) == 1)
        {
            forceOpposing.y = -particle.velocity.y;
        }

        var forceFrictionKinetic = ForceGenerator.FrictionKinetic(collision2D.impulse, forceOpposing, particle.frictionCoefficientKinetic);
        particle.AddForce(forceFrictionKinetic);

        var momentArm = collision2D.contact2D.point - particle.position;
        float impulseTorque = (momentArm.x * forceFrictionKinetic.y) - (momentArm.y * forceFrictionKinetic.x);
        Debug.Log(gameObject.name);
        particle.angularVelocity += particle.inertiaInv * impulseTorque;

        if (Math.Abs(particle.velocity.sqrMagnitude) < 0.05f)
        {
            particle.angularVelocity = 0;
        }
    }
}
