﻿using System;
using UnityEngine;
using Rigidbody = Yeet.Rigidbody;

public class DemoScript : MonoBehaviour
{
    private Rigidbody rb;
    private Vector3 springAnchor;

    public Vector3 platformSurfaceNormal;
    public float springLength = 0.001f;
    public float springStiffnessCoeff = 5.0f;
    public float frictionCoeffStatic = 0.5f;
    public float frictionCoeffKinetic = 0.5f;
    public Vector3 fluidVelocity;
    public float fluidDensity;
    public float dragCoeff;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        springAnchor = rb.transform.position;
    }

    private void FixedUpdate()
    {
        Vector3 forceGravity = ForceGenerator.Gravity(rb.Mass, -9.81f, Vector3.up);
        Vector3 forceNormal = ForceGenerator.Normal(forceGravity, platformSurfaceNormal);
        Vector3 forceSliding = ForceGenerator.Sliding(forceGravity, forceNormal);
        rb.AddForce(forceSliding);
        Vector3 forceStaticFriction = ForceGenerator.FrictionStatic(forceNormal, forceSliding, frictionCoeffStatic);
        rb.AddForce(forceStaticFriction);
        Vector3 forceKineticFriction = ForceGenerator.FrictionKinetic(forceNormal, rb.velocity, frictionCoeffKinetic);
        rb.AddForce(forceKineticFriction);
        Debug.DrawLine(springAnchor, rb.position, Color.red);
        rb.AddForce(ForceGenerator.Spring(rb.position, springAnchor, springLength, springStiffnessCoeff));
    }
}
